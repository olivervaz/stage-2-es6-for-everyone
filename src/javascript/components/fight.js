import { controls } from '../../constants/controls';
import { showAlertModal } from './modal/alert';

const CRITICAL_HIT_TIMEOUT = 10;
const CRITICAL_HIT_TIMEOUT_MILLISECONDS = 10000;
const SECOND_IN_MILLISECONDS = 1000;
const CRITICAL_HIT_RATIO = 2;

export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {
    const firstFighterParams = updatePlayer(firstFighter, 'left');
    const secondFighterParams = updatePlayer(secondFighter, 'right');
    const fightActions = new Set();

    const onKeyDown = (control) => {
      const winner = defineWinner(firstFighterParams, secondFighterParams);
      if (winner) {
        resolve(winner);
      } else {
        fightActions.add(control);
        fightHandler(firstFighterParams, secondFighterParams, fightActions);
      }
    };

    const onKeyUp = (control) => {
      fightActions.delete(control);
    };

    document.addEventListener('keydown', event => onKeyDown(event.code));
    document.addEventListener('keyup', event => onKeyUp(event.code));
  });
}

export function defineWinner(firstFighter, secondFighter) {
  const healthFirst = firstFighter.currentHealth;
  const healthSecond = secondFighter.currentHealth;

  if (healthFirst <= 0 || healthSecond <= 0) {
    return healthFirst <= 0 ? secondFighter : firstFighter;
  }
}

export function updatePlayer(fighter, position) {
  const initialTime = Date.now() - CRITICAL_HIT_TIMEOUT_MILLISECONDS;
  return {
    ...fighter,
    currentHealth: fighter.health,
    criticalDamageUsedTime: initialTime,
    isBlocked: false,
    barId: `${position}-fighter-indicator`
  };
}

export function fightHandler(fighterFirst, fighterSecond, fightActions) {
  switch (true) {
    case fightActions.has(controls.PlayerOneAttack):
    case controls.PlayerOneCriticalHitCombination.every(key => fightActions.has(key)):

      if(!fightActions.has(controls.PlayerOneBlock)) {
        attackController(fighterFirst, fighterSecond, fightActions);
      } else {
        showAlertModal(fighterFirst, 'You cannot attack with block');
      }

      break;
    case fightActions.has(controls.PlayerTwoAttack):
    case controls.PlayerTwoCriticalHitCombination.every(key => fightActions.has(key)):

      if(!fightActions.has(controls.PlayerTwoBlock)) {
        attackController(fighterSecond, fighterFirst, fightActions);
      } else {
        showAlertModal(fighterFirst, 'You cannot attack with block');
      }
      break;
  }
}

export function attackController(attacker, defender, fightActions) {
  const isBlocked = (fightActions.has(controls.PlayerOneBlock)
    || fightActions.has(controls.PlayerTwoBlock));

  switch (true) {
    case controls.PlayerOneCriticalHitCombination.every(key => fightActions.has(key)):
    case controls.PlayerTwoCriticalHitCombination.every(key => fightActions.has(key)):
      defender.currentHealth -= criticalAttackHandler(attacker);
      break;
    default:
      defender.currentHealth -= damageHandler(attacker, defender, isBlocked);
  }

  changeHealthBar(defender);
}

export function damageHandler(attacker, defender, isBlocked) {
  const { attack } = attacker;
  let damage = getDamage(attacker, defender);

  if (isBlocked) {
    damage = 0;
  }

  return damage;
}

export function criticalAttackHandler(attacker) {
  const { criticalDamageUsedTime } = attacker;
  const secondsSinceLastUsage = (Date.now() - criticalDamageUsedTime) / SECOND_IN_MILLISECONDS;
  let damage = 0;

  if (secondsSinceLastUsage > CRITICAL_HIT_TIMEOUT) {
    damage = getCriticalDamage(attacker);
    attacker.criticalDamageUsedTime = Date.now();
  } else {
    showAlertModal(attacker, 'You cannot use critical attack use more than per 10 second');
  }

  return damage;
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower - blockPower;

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;

  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;

  return defense * dodgeChance;
}

export function getCriticalDamage(fighter) {
  const { attack } = fighter;
  return attack * CRITICAL_HIT_RATIO;
}

export function changeHealthBar(fighter) {
  const { health, currentHealth, barId } = fighter;
  const percentageLeft = currentHealth * 100 / health;
  const bar = document.getElementById(barId);

  bar.style.width = `${percentageLeft}%`;
}
