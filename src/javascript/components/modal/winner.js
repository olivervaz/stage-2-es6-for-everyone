import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  showModal(winnerModal(fighter));
}

export function winnerModal(fighter) {
  const {name} = fighter;
  const winnerBodyElement = createElement({tagName: 'div', className:'winner___body'});
  const imgElement = createFighterImage(fighter);
  winnerBodyElement.append(imgElement);

  return {
    title :`Winner: ${name}`,
    bodyElement: winnerBodyElement,
    onClose: onCloseWinnerModal,
  };
}

export function onCloseWinnerModal() {
  document.location.reload(true);
}

