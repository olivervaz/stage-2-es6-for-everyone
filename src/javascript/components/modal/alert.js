import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showAlertModal(fighter,message) {
  showModal(alertModal(fighter,message));
}

export function alertModal(fighter,message) {
  const {name} = fighter;
  const alertBodyElement = createElement({tagName: 'div', className:'modal-body'});
  alertBodyElement.innerText = message;
  return {
    title :`Alert for ${name}`,
    bodyElement: alertBodyElement
  };
}
