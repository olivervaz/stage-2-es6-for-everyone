import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });
  if (fighter) {
    const fightersList = document.querySelector('.fighters___list');
    const imgElement = createFighterImage(fighter);
    const paramsElement = getFighterParamsElement(fighter);
    fighterElement.append(imgElement, paramsElement);
  }
  return fighterElement;
}

export function getFighterParamsElement(fighter) {
  const paramsElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });
  const fighterParams = getFighterPreviewParams(fighter)
    .map(([key, value]) => `${key}: ${value}`);
  const params = fighterParams.map(key => createElement({
    tagName: 'p',
    className: 'fighter-preview___info-item'
  }));
  params.forEach((item, index) => item.textContent = fighterParams[index]);
  paramsElement.append(...params);

  return paramsElement;
}

export function getFighterPreviewParams(fighter) {
  const paramsToDisplay = ['name', 'health', 'attack', 'defense'];
  const fighterParams = Object.keys(fighter)
    .filter(key => paramsToDisplay.includes(key))
    .reduce((res, key) => Object.assign(res, { [key]: fighter[key] }), {});

  return Object.entries(fighterParams);
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
